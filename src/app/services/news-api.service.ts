import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsApiService {

  constructor(private http: HttpClient) { }

  getArticles() {
    return this.http.get(`${environment.API_URL}/news`);
  }

  getSources() {
    return this.http.get(`${environment.API_URL}/news/sources`);
  }

  getArticlesBySource(source) {
    return this.http.get(`${environment.API_URL}/news/${source}`);
  }
}
