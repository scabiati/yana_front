import { Component, OnInit } from '@angular/core';
import {NewsApiService} from '../../services/news-api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styles: [
  ]
})
export class ResultComponent implements OnInit {

  public Articles: any;
  private sourceId: string;


  constructor(
    private newsApiService: NewsApiService,
    public route: ActivatedRoute,
  ) {}

  ngOnInit(): void {

    this.route.queryParams.subscribe((queryParams) => {
      this.sourceId = queryParams.source;
      this.newsApiService.getArticlesBySource(this.sourceId).subscribe((response: any) => {
        this.Articles = response.data.articles;
      });
    });
  }

}
