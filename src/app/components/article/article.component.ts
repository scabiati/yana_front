import {Component, Input, OnInit, DoCheck, SimpleChanges, OnChanges} from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styles: [
  ]
})
export class ArticleComponent implements OnInit, OnChanges {

  @Input() Articles: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

}
