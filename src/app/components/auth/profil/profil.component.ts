import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styles: []
})
export class ProfilComponent implements OnInit {
  public currentUser: any;

  constructor(
    private authService: AuthService,
  ) { }

  async ngOnInit() {
    const test = await this.authService.getCurrentUser().subscribe((currentUser) => {
      this.currentUser = currentUser;
    });
  }

  logout() {
    this.authService.logout();
  }


}
