import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {NewsApiService} from '../../services/news-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  public keyword = null;
  public sourceSelected = null;
  public Sources: Array<any> = [];
  public Articles: any = null;

  @Output() buttonClicked: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    public newsApiService: NewsApiService,
    private router: Router,
  ) { }

  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {

    this.newsApiService.getSources().subscribe((response: any) => {
      this.Sources = response.data.sources;
    });

    this.form = new FormGroup({
      source: new FormControl(this.sourceSelected),
    });
  }

  search() {
    localStorage.setItem('sourceSelected', this.form.value.source);
    this.router.navigate(['/result'], { queryParams: { source: this.form.value.source} });
  }

}
