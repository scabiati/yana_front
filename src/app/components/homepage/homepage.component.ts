import { Component, OnInit } from '@angular/core';

import {NewsApiService} from '../../services/news-api.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styles: [
  ]
})
export class HomepageComponent implements OnInit {

  public Articles: any;

  constructor(
    private newsApiService: NewsApiService,
  ) {}

  ngOnInit(): void {

    this.newsApiService.getArticles().subscribe((response: any) => {
      console.log(response);
      this.Articles = response.data.articles;
    });

  }

}
