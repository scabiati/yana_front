import { Routes, RouterModule } from '@angular/router';
import {HomepageComponent} from './components/homepage/homepage.component';
import {SearchComponent} from './components/search/search.component';
import {ResultComponent} from './components/result/result.component';

export const AppRoutingModule: Routes = [
  { path: 'search', component: SearchComponent },
  { path: 'result', component: ResultComponent },
  { path: 'auth', loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule)},
  { path: '', component: HomepageComponent, pathMatch: 'full' },
];
